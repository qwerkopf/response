

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup

    // MAILGUN LOGIN
    process.env.MAIL_URL = 'smtp://postmaster%40sandboxa52d784aa044443a9fc33d136ad93fe4.mailgun.org:9b187f5c4991b42a9cc52f15eee43e99@smtp.mailgun.org';


  });
}


Schema = {};
Schema.contact = new SimpleSchema({
    name: {
        type: String,
        label: "Your name",
        max: 50
    },
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        label: "E-mail address"
    },
    message: {
        type: String,
        label: "Message",
        max: 1000
    }
});


if(Meteor.isClient) {
Template.contactForm.helpers({
  contactFormSchema: function() {
    return Schema.contact;
  }
});
}

Meteor.methods({
  sendEmail: function(doc) {
    // Important server-side check for security and data integrity
    check(doc, Schema.contact);

    // Build the e-mail text
    var text = "Name: " + doc.name + "\n\n"
            + "Email: " + doc.email + "\n\n\n\n"
            + doc.message;

    this.unblock();

    // Send the e-mail
    Email.send({
        to: "sebastian.keusch@gmail.com",
        from: doc.email,
        subject: "Website Contact Form - Message From " + doc.name,
        text: text
    });
  }
});